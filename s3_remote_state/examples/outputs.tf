output "s3_bucket_arn" {
  value       = module.s3_remote_state.s3_bucket_arn
  description = "The Amazon Resource Name (ARN) of the S3 bucket"
}

output "dynamodb_table_name" {
  value       = module.s3_remote_state.dynamodb_table_name
  description = "The name of the DynamoDB table"
}
