data "aws_vpc" "selected" {
  default = true
}

output "default_vpc" {
  value = data.aws_vpc.selected.id
}
